module sram_mem (
    reset_bar_in,
    clock_in,
    enable_in,
    address_in,
    read_data_out,
    write_data_in,
    read_write_bar_in,
    read_complete_out,
    write_complete_out
);
    parameter ADDR_WIDTH = 10;
    input               reset_bar_in;
    input               clock_in;
    input               enable_in;
    input       [31:0]  address_in;
    output reg  [31:0]  read_data_out;
    input       [31:0]  write_data_in;
    input               read_write_bar_in;
    output              read_complete_out;
    output              write_complete_out;
    
    reg         [31:0]  sram_memory[1023:0];
    
    reg          read_complete;
    reg          write_complete;
 
    reg    [2:0] state;
    reg    [2:0] next_state;
    localparam  IDLE            = 3'd0,
                READ_WRITE_MODE = 3'd1;

    assign write_complete_out   = enable_in & write_complete;
    assign read_complete_out    = enable_in & read_complete;

    always @ (posedge clock_in or negedge reset_bar_in) begin
        if (reset_bar_in==0) begin
            read_data_out <= 32'd0;
            read_complete <= 32'd0;
            write_complete <= 32'd0;
            state <= IDLE;
        end else begin
            case (state)
                IDLE: begin
                    if (enable_in) begin
                        state <= READ_WRITE_MODE;
                    end else begin
                        state <= IDLE;
                    end
                end
                READ_WRITE_MODE: begin
                    if (read_write_bar_in) begin
                        read_data_out <= sram_memory[address_in[ADDR_WIDTH-1:2]];
                        read_complete <= 1'd1;
                    end else begin
                        sram_memory[address_in[ADDR_WIDTH-1:2]] <= write_data_in;
                        write_complete <= 1'd1;
                    end
                    if (enable_in) begin
                        state <= READ_WRITE_MODE;
                    end else begin
                        read_complete <= 1'd0;
                        write_complete <= 1'd0;
                        state <= IDLE;
                    end
                end
            endcase
        end
    end

endmodule


