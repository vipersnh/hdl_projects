`include "./axi2sram.v"
`include "./axi_driver_tb.v"
`include "../l1_counter/l1_counter.v"
`include "../l1_counter/l1_counter_regbank.v"

module axi2sram_l1_counter_tb;

    /* Interface connections between axi driver and axi2sram module */
    /* AXI Global System Signals */
    wire                    s_axi_aclk;
    wire                    s_axi_aresetn;

    /* AXI Write Address Channel Signals */
    wire            [31:0]  s_axi_awaddr;
    wire            [2:0]   s_axi_awprot;
    wire                    s_axi_awvalid;
    wire                    s_axi_awready;

    /* AXI Write Data Channel Signals */
    wire            [31:0]  s_axi_wdata;
    wire            [3:0]   s_axi_wstrb;
    wire                    s_axi_wvalid;
    wire                    s_axi_wready;

    /* AXI Write Response Channel Signals */
    wire            [1:0]   s_axi_bresp;
    wire                    s_axi_bvalid;
    wire                    s_axi_bready;

    /* AXI Read Address Channel Signals */
    wire            [31:0]  s_axi_araddr;
    wire            [2:0]   s_axi_arprot;
    wire                    s_axi_arvalid;
    wire                    s_axi_arready;

    /* AXI Read Data Channel Signals */
    wire            [31:0]  s_axi_rdata;
    wire            [1:0]   s_axi_rresp;
    wire                    s_axi_rvalid;
    wire                    s_axi_rready;

    /* Interface connections between axi2sram and sram protocol modules */
    wire                i_reset_bar;
    wire                i_clock;
    wire                i_enable;
    wire        [31:0]  i_address;
    wire        [31:0]  i_read_data;
    wire        [31:0]  i_write_data;
    wire                i_read_write_bar;
    wire                i_read_complete;
    wire                i_write_complete;

    /* Instantiate axi_driver_tb module */
    axi_driver_tb inst0_axi_driver (
        .s_axi_aclk             (s_axi_aclk),
        .s_axi_aresetn          (s_axi_aresetn),

        .s_axi_awaddr           (s_axi_awaddr),
        .s_axi_awprot           (),
        .s_axi_awvalid          (s_axi_awvalid),
        .s_axi_awready          (s_axi_awready),

        .s_axi_wdata            (s_axi_wdata),
        .s_axi_wstrb            (s_axi_wstrb),
        .s_axi_wvalid           (s_axi_wvalid),
        .s_axi_wready           (s_axi_wready),

        .s_axi_bresp            (s_axi_bresp),
        .s_axi_bvalid           (s_axi_bvalid),
        .s_axi_bready           (s_axi_bready),

        .s_axi_araddr           (s_axi_araddr),
        .s_axi_arprot           (),
        .s_axi_arvalid          (s_axi_arvalid),
        .s_axi_arready          (s_axi_arready),

        .s_axi_rdata            (s_axi_rdata),
        .s_axi_rresp            (s_axi_rresp),
        .s_axi_rvalid           (s_axi_rvalid),
        .s_axi_rready           (s_axi_rready)
    );

    /* Instantiate axi2sram module */
    axi2sram    inst0_axi2sram (
        .s_axi_aclk             (s_axi_aclk),
        .s_axi_aresetn          (s_axi_aresetn),

        .s_axi_awaddr           (s_axi_awaddr),
        .s_axi_awprot           (),
        .s_axi_awvalid          (s_axi_awvalid),
        .s_axi_awready          (s_axi_awready),

        .s_axi_wdata            (s_axi_wdata),
        .s_axi_wstrb            (s_axi_wstrb),
        .s_axi_wvalid           (s_axi_wvalid),
        .s_axi_wready           (s_axi_wready),

        .s_axi_bresp            (s_axi_bresp),
        .s_axi_bvalid           (s_axi_bvalid),
        .s_axi_bready           (s_axi_bready),

        .s_axi_araddr           (s_axi_araddr),
        .s_axi_arprot           (),
        .s_axi_arvalid          (s_axi_arvalid),
        .s_axi_arready          (s_axi_arready),

        .s_axi_rdata            (s_axi_rdata),
        .s_axi_rresp            (s_axi_rresp),
        .s_axi_rvalid           (s_axi_rvalid),
        .s_axi_rready           (s_axi_rready),

        .i_reset_bar_out          (i_reset_bar),
        .i_clock_out              (i_clock),
        .i_enable_out             (i_enable),
        .i_address_out            (i_address),
        .i_read_data_in           (i_read_data),
        .i_write_data_out         (i_write_data),
        .i_read_write_bar_out     (i_read_write_bar),
        .i_read_complete_in       (i_read_complete),
        .i_write_complete_in      (i_write_complete)
    );
    
    /* Instantiate l1_counter module */
    l1_counter_regbank inst0_l1_counter(
        .reset_bar_in           (i_reset_bar),
        .clock_in               (i_clock),
        .enable_in              (i_enable),
        .address_in             (i_address),
        .read_data_out          (i_read_data),
        .write_data_in          (i_write_data),
        .read_write_bar_in      (i_read_write_bar),
        .read_complete_out      (i_read_complete),
        .write_complete_out     (i_write_complete)
    );

    initial begin
        $dumpfile("axi2sram_l1_counter_tb.vcd");
        $dumpvars(2, axi2sram_l1_counter_tb);
        inst0_axi_driver.signal_reset();
        inst0_axi_driver.axi_write_transaction(32'h0, 32'hFFFFFFFF);
        #10 inst0_axi_driver.axi_read_transaction(32'd24);
        #10 inst0_axi_driver.axi_read_transaction(32'd24);
        #10 inst0_axi_driver.axi_read_transaction(32'd24);
        #10 inst0_axi_driver.axi_read_transaction(32'd24);
        #10 inst0_axi_driver.axi_read_transaction(32'd24);
        #10 inst0_axi_driver.axi_read_transaction(32'd24);
        #200 $finish;
    end
endmodule

