module axi2sram (
    /* AXI Global System Signals */
    s_axi_aclk,
    s_axi_aresetn,

    /* AXI Write Address Channel Signals */
    s_axi_awaddr,
    s_axi_awprot,
    s_axi_awvalid,
    s_axi_awready,

    /* AXI Write Data Channel Signals */
    s_axi_wdata,
    s_axi_wstrb,
    s_axi_wvalid,
    s_axi_wready,

    /* AXI Write Response Channel Signals */
    s_axi_bresp,
    s_axi_bvalid,
    s_axi_bready,

    /* AXI Read Address Channel Signals */
    s_axi_araddr,
    s_axi_arprot,
    s_axi_arvalid,
    s_axi_arready,

    /* AXI Read Data Channel Signals */
    s_axi_rdata,
    s_axi_rresp,
    s_axi_rvalid,
    s_axi_rready,

    /* SRAM Protocol */
    i_reset_bar_out,
    i_clock_out,
    i_enable_out,
    i_address_out,
    i_read_data_in,
    i_write_data_out,
    i_read_write_bar_out,
    i_read_complete_in,
    i_write_complete_in
);


    /* AXI Global System Signals */
    input                   s_axi_aclk;
    input                   s_axi_aresetn;

    /* AXI Write Address Channel Signals */
    input           [31:0]  s_axi_awaddr;
    input            [2:0]  s_axi_awprot;
    input                   s_axi_awvalid;
    output reg              s_axi_awready;

    /* AXI Write Data Channel Signals */
    input           [31:0]  s_axi_wdata;
    input            [3:0]  s_axi_wstrb;
    input                   s_axi_wvalid;
    output reg              s_axi_wready;

    /* AXI Write Response Channel Signals */
    output           [1:0]  s_axi_bresp;
    output reg              s_axi_bvalid;
    input                   s_axi_bready;

    /* AXI Read Address Channel Signals */
    input           [31:0]  s_axi_araddr;
    input            [2:0]  s_axi_arprot;
    input                   s_axi_arvalid;
    output reg              s_axi_arready;

    /* AXI Read Data Channel Signals */
    output reg      [31:0]  s_axi_rdata;
    output           [1:0]  s_axi_rresp;
    output reg              s_axi_rvalid;
    input                   s_axi_rready;


    /* SRAM Protocol */
    output                  i_reset_bar_out;
    output                  i_clock_out;
    output reg              i_enable_out;
    output reg      [31:0]  i_address_out;
    input           [31:0]  i_read_data_in;
    output reg      [31:0]  i_write_data_out;
    output reg              i_read_write_bar_out;
    input                   i_read_complete_in;
    input                   i_write_complete_in;


    assign  i_reset_bar_out = s_axi_aresetn;
    assign  i_clock_out = s_axi_aclk;
    
    assign  s_axi_bresp = 2'd0;
    assign  s_axi_rresp = 2'd0;

    reg     [2:0]   read_state;

    localparam  READ_IDLE               = 4'd0,
                READ_DATA_WAIT_SRAM     = 4'd1,
                READ_DATA_EXPECT_RREADY = 4'd2;

    reg     [3:0]   write_state;

    localparam  WRITE_IDLE              = 4'd0,
                WRITE_DATA_WAIT_AXI     = 4'd1,
                WRITE_DATA_WAIT_SRAM    = 4'd2,
                WRITE_RESP_EXPECT_RREADY= 4'd3;

    reg     [1:0]   axi_state;
    reg     [1:0]   next_axi_state;

    localparam AXI_IDLE         = 2'd0,
               AXI_READ_MODE    = 2'd1,
               AXI_WRITE_MODE   = 2'd2;

    /* read_state update logic */
    always @ (posedge s_axi_aclk or negedge s_axi_aresetn) begin
        if (!s_axi_aresetn) begin
            axi_state <= AXI_IDLE;
            s_axi_awready <= 1'd1;
            s_axi_arready <= 1'd1;
        end else begin
            axi_state <= next_axi_state;
            case (next_axi_state)
                AXI_IDLE: begin
                    s_axi_awready <= 1'd1;
                    s_axi_arready <= 1'd1;
                end
                default: begin
                    s_axi_awready <= 1'd0;
                    s_axi_arready <= 1'd0;
                end
            endcase
        end
    end

    always @ (write_state or read_state) begin
        if (write_state != WRITE_IDLE) begin
            next_axi_state = AXI_WRITE_MODE;
        end else if (read_state != READ_IDLE) begin
            next_axi_state = AXI_READ_MODE;
        end else begin
            next_axi_state = AXI_IDLE;
        end
    end

    /* next_read_state determination combinatorial logic */
    always @ (posedge s_axi_aclk) begin
        if (s_axi_aresetn) begin
            case (axi_state)
                AXI_IDLE: begin
                    if (s_axi_arvalid) begin
                        read_state <= READ_DATA_WAIT_SRAM;
                        i_address_out <= s_axi_araddr;
                    end else if (s_axi_awvalid) begin
                        write_state <= WRITE_DATA_WAIT_AXI;
                        i_address_out <= s_axi_awaddr;
                    end
                end
                AXI_READ_MODE: begin
                    case (read_state)
                        READ_IDLE: begin end
                        READ_DATA_WAIT_SRAM: begin
                            i_enable_out <= 1'd1;
                            i_read_write_bar_out <= 1'd1;
                            if (i_read_complete_in) begin
                                s_axi_rdata <= i_read_data_in;
                                s_axi_rvalid <= 1'd1;
                                read_state <= READ_DATA_EXPECT_RREADY;
                            end else begin
                                read_state <= READ_DATA_WAIT_SRAM;
                            end
                        end
                        READ_DATA_EXPECT_RREADY: begin
                            i_enable_out <= 1'd0;
                            if (s_axi_rready) begin
                                s_axi_rvalid <= 1'd0;
                                read_state <= READ_IDLE;
                            end else begin
                                read_state <= READ_DATA_EXPECT_RREADY;
                            end
                        end
                        default: read_state <= READ_IDLE;
                    endcase
                end
                AXI_WRITE_MODE: begin
                    case (write_state)
                        WRITE_IDLE: begin end
                        WRITE_DATA_WAIT_AXI: begin
                            if (s_axi_wvalid) begin
                                s_axi_wready <= 1'd1;
                                i_write_data_out <= s_axi_wdata;
                                write_state <= WRITE_DATA_WAIT_SRAM;
                            end else begin
                                write_state <= WRITE_DATA_WAIT_AXI;
                            end
                        end
                        WRITE_DATA_WAIT_SRAM: begin
                            s_axi_wready <= 1'd0;
                            i_enable_out <= 1'd1;
                            i_read_write_bar_out <= 1'd0;
                            if (i_write_complete_in) begin
                                s_axi_bvalid <= 1'd1;
                                write_state <= WRITE_RESP_EXPECT_RREADY;
                            end else begin
                                write_state <= WRITE_DATA_WAIT_SRAM;
                            end
                        end
                        WRITE_RESP_EXPECT_RREADY: begin
                            i_enable_out <= 1'd0;
                            if (s_axi_bready) begin
                                s_axi_bvalid <= 1'd0;
                                write_state <= WRITE_IDLE;
                            end else begin
                                write_state <= WRITE_RESP_EXPECT_RREADY;
                            end
                        end
                    endcase
                end
            endcase
        end else begin
            s_axi_wready <= 1'd0;
            s_axi_bvalid <= 1'd0;

            s_axi_rdata <= 32'd0;
            s_axi_rvalid <= 1'd0;

            read_state <= READ_IDLE;
            write_state <= WRITE_IDLE;
            i_enable_out <= 1'd0;
            i_address_out <= 32'd0;
            i_write_data_out <= 32'd0;
            i_read_write_bar_out <= 32'd0;
        end
    end
endmodule

