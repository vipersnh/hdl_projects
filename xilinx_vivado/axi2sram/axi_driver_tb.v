`timescale 10ns/1ns

module axi_driver_tb (
    /* AXI Global System Signals */
    s_axi_aclk,
    s_axi_aresetn,

    /* AXI Write Address Channel Signals */
    s_axi_awaddr,
    s_axi_awprot,
    s_axi_awvalid,
    s_axi_awready,

    /* AXI Write Data Channel Signals */
    s_axi_wdata,
    s_axi_wstrb,
    s_axi_wvalid,
    s_axi_wready,

    /* AXI Write Response Channel Signals */
    s_axi_bresp,
    s_axi_bvalid,
    s_axi_bready,

    /* AXI Read Address Channel Signals */
    s_axi_araddr,
    s_axi_arprot,
    s_axi_arvalid,
    s_axi_arready,

    /* AXI Read Data Channel Signals */
    s_axi_rdata,
    s_axi_rresp,
    s_axi_rvalid,
    s_axi_rready,
);

    /* AXI Global System Signals */
    output reg              s_axi_aclk;
    output reg              s_axi_aresetn;

    /* AXI Write Address Channel Signals */
    output reg      [31:0]  s_axi_awaddr;
    output reg       [2:0]  s_axi_awprot;
    output reg              s_axi_awvalid;
    input                   s_axi_awready;

    /* AXI Write Data Channel Signals */
    output reg          [31:0]  s_axi_wdata;
    output reg           [3:0]  s_axi_wstrb;
    output reg                  s_axi_wvalid;
    input                   s_axi_wready;

    /* AXI Write Response Channel Signals */
    input            [1:0]  s_axi_bresp;
    input                   s_axi_bvalid;
    output reg              s_axi_bready;

    /* AXI Read Address Channel Signals */
    output reg      [31:0]  s_axi_araddr;
    output reg       [2:0]  s_axi_arprot;
    output reg              s_axi_arvalid;
    input                   s_axi_arready;

    /* AXI Read Data Channel Signals */
    input           [31:0]  s_axi_rdata;
    input            [1:0]  s_axi_rresp;
    input                   s_axi_rvalid;
    output reg              s_axi_rready;


    task signal_reset;
        begin
            #0  s_axi_aresetn = 0;
            #10 s_axi_aresetn = 1;
        end
    endtask

    task axi_read_transaction;
        input   [31:0]  read_address;
        fork
            begin
                /* Complete axi ARADDR transaction */
                #0 s_axi_araddr = read_address;
                #0 s_axi_arvalid = 1'b1;
                @ (posedge s_axi_aclk);
                while (s_axi_arready==0)
                    @ (posedge s_axi_aclk);
                #0 s_axi_arvalid = 1'b0;
                #0 s_axi_araddr = 32'd0;
            end

            begin
                #0 s_axi_rready = 1'b1;
                /* Complete axi RDATA transaction */
                @ (posedge s_axi_aclk);
                while (s_axi_rvalid==0) begin
                    @ (posedge s_axi_aclk);
                end
                #0 s_axi_rready = 1'b0;
            end
        join
    endtask

    task axi_write_transaction;
        input   [31:0]  write_address;
        input   [31:0]  write_data;

        fork
            begin
                /* Complete axi AWADDR transaction */
                #0 s_axi_awaddr = write_address;
                @ (posedge s_axi_aclk);  /* Wait for clock to be high */
                #0 s_axi_awvalid = 1'b1;

                @ (posedge s_axi_aclk);
                while (s_axi_awready==0)
                    @ (posedge s_axi_aclk);
                #0 s_axi_awvalid = 1'b0;
                #0 s_axi_awaddr = 32'd0;
            end

            begin
                /* Complete axi WDATA transaction */
                #0 s_axi_wdata      = write_data;
                #0 s_axi_wvalid     = 1'b1;

                /* Wait for write data to be accepted */
                @ (posedge s_axi_aclk);
                while (s_axi_wready==0)
                    @ (posedge s_axi_aclk);
                #0 s_axi_wvalid     = 1'b0;
            end

            begin
                #0 s_axi_bready     = 1'b1;

                /* Wait for bvalid signal */
                @ (posedge s_axi_aclk);
                while (s_axi_bvalid==0)
                    @ (posedge s_axi_aclk);
                #0 s_axi_bready     = 1'b0;
            end
        join
    endtask

    initial begin
        #0 s_axi_aresetn = 0;
        #0 s_axi_aclk = 0;
        #0 s_axi_wdata = 0;
        #0 s_axi_araddr = 0;
        #0 s_axi_awaddr = 0;
        #0 s_axi_arvalid = 0;
        #0 s_axi_awvalid = 0;
        #0 s_axi_bready = 0;
        #0 s_axi_rready = 0;
        #0 s_axi_wvalid = 0;
        #0 s_axi_wstrb = 0;
    end

    assign i_clock = s_axi_aclk;
    assign i_reset_bar = s_axi_aresetn;

    /* Make a regular pulsing clock. */
    always #1 s_axi_aclk = !s_axi_aclk;
endmodule
