`timescale 1ps/1ps

/* Module Definition */
module zynq_fpga_top (
    /* DDR Interface */
    inout  wire  [14:0] DDR_addr,
    inout  wire   [2:0] DDR_ba,
    inout  wire         DDR_cas_n,
    inout  wire         DDR_ck_n,
    inout  wire         DDR_ck_p,
    inout  wire         DDR_cke,
    inout  wire         DDR_cs_n,
    inout  wire   [3:0] DDR_dm,
    inout  wire  [31:0] DDR_dq,
    inout  wire   [3:0] DDR_dqs_n,
    inout  wire   [3:0] DDR_dqs_p,
    inout  wire         DDR_odt,
    inout  wire         DDR_ras_n,
    inout  wire         DDR_reset_n,
    inout  wire         DDR_we_n,
    
    /* Custom outputs */
    output wire         zed_board_led_0,
    output wire         zed_board_led_1,
    output wire         zed_board_led_2,
    output wire         zed_board_led_3,
    output wire         zed_board_led_4,
    output wire         zed_board_led_5,
    output wire         zed_board_led_6,
    output wire         zed_board_led_7,
    
    /* FIXED IO INTF */
    inout  wire         FIXED_IO_ddr_vrn,
    inout  wire         FIXED_IO_ddr_vrp,
    inout  wire  [53:0] FIXED_IO_mio,
    inout  wire         FIXED_IO_ps_clk,
    inout  wire         FIXED_IO_ps_porb,
    inout  wire         FIXED_IO_ps_srstb,
    
    input  wire         ext_reset_in
);

    localparam ADDR_W = 32;

    /* SRAM Protocol */
    (* mark_debug = "true" *) wire                        i_reset_bar;
    (* mark_debug = "true" *) wire                        i_clock;
    (* mark_debug = "true" *) wire                        i_enable;
    (* mark_debug = "true" *) wire          [ADDR_W-1:0]  i_address;
    (* mark_debug = "true" *) wire                [31:0]  i_read_data;
    (* mark_debug = "true" *) wire                [31:0]  i_write_data;
    (* mark_debug = "true" *) wire                        i_read_write_bar;
    (* mark_debug = "true" *) wire                        i_read_complete;
    (* mark_debug = "true" *) wire                        i_write_complete;

    reg                  [26:0] up_counter;
    
    /* AXI4 LITE */
    (* mark_debug = "true" *) wire                        clk;
    (* mark_debug = "true" *) wire                        rst_n;
    (* mark_debug = "true" *) wire                        s_axi_awvalid;
    (* mark_debug = "true" *) wire                        s_axi_awready;
    (* mark_debug = "true" *) wire                        s_axi_wvalid;
    (* mark_debug = "true" *) wire                        s_axi_wready;
    (* mark_debug = "true" *) wire                        s_axi_bvalid;
    (* mark_debug = "true" *) wire                        s_axi_bready;
    (* mark_debug = "true" *) wire                        s_axi_arvalid;
    (* mark_debug = "true" *) wire                        s_axi_arready;
    (* mark_debug = "true" *) wire                        s_axi_rvalid;
    (* mark_debug = "true" *) wire                        s_axi_rready;
    (* mark_debug = "true" *) wire                [31:00] s_axi_awaddr;
    (* mark_debug = "true" *) wire                [31:00] s_axi_wdata;
    (* mark_debug = "true" *) wire                [03:00] s_axi_wstrb;
    (* mark_debug = "true" *) wire                [01:00] s_axi_bresp;
    (* mark_debug = "true" *) wire                [31:00] s_axi_araddr;
    (* mark_debug = "true" *) wire                [31:00] s_axi_rdata;
    (* mark_debug = "true" *) wire                [01:00] s_axi_rresp;

    design_1_wrapper design_1_i (
        .DDR_addr                 (DDR_addr),
        .DDR_ba                   (DDR_ba),
        .DDR_cas_n                (DDR_cas_n),
        .DDR_ck_n                 (DDR_ck_n),
        .DDR_ck_p                 (DDR_ck_p),
        .DDR_cke                  (DDR_cke),
        .DDR_cs_n                 (DDR_cs_n),
        .DDR_dm                   (DDR_dm),
        .DDR_dq                   (DDR_dq),
        .DDR_dqs_n                (DDR_dqs_n),
        .DDR_dqs_p                (DDR_dqs_p),
        .DDR_odt                  (DDR_odt),
        .DDR_ras_n                (DDR_ras_n),
        .DDR_reset_n              (DDR_reset_n),
        .DDR_we_n                 (DDR_we_n),
        .FCLK_CLK0                (clk),
        .FCLK_RESET0_N            (rst_n),
        .FIXED_IO_ddr_vrn         (FIXED_IO_ddr_vrn),
        .FIXED_IO_ddr_vrp         (FIXED_IO_ddr_vrp),
        .FIXED_IO_mio             (FIXED_IO_mio),
        .FIXED_IO_ps_clk          (FIXED_IO_ps_clk),
        .FIXED_IO_ps_porb         (FIXED_IO_ps_porb),
        .FIXED_IO_ps_srstb        (FIXED_IO_ps_srstb),
        
        .M00_AXI_araddr           (s_axi_araddr),
        .M00_AXI_arprot           (), // OPEN
        .M00_AXI_arready          (s_axi_arready),
        .M00_AXI_arvalid          (s_axi_arvalid),
        .M00_AXI_awaddr           (s_axi_awaddr),
        .M00_AXI_awprot           (), // OPEN
        .M00_AXI_awready          (s_axi_awready),
        .M00_AXI_awvalid          (s_axi_awvalid),
        .M00_AXI_bready           (s_axi_bready),
        .M00_AXI_bresp            (s_axi_bresp),
        .M00_AXI_bvalid           (s_axi_bvalid),
        .M00_AXI_rdata            (s_axi_rdata),
        .M00_AXI_rready           (s_axi_rready),
        .M00_AXI_rresp            (s_axi_rresp),
        .M00_AXI_rvalid           (s_axi_rvalid),
        .M00_AXI_wdata            (s_axi_wdata),
        .M00_AXI_wready           (s_axi_wready),
        .M00_AXI_wstrb            (s_axi_wstrb),
        .M00_AXI_wvalid           (s_axi_wvalid)
    );
   
    /* AXI4-Lite to SRAM convertor  */
    axi2sram    inst0_axi2sram (
        /* AXI Global System Signals */
        .s_axi_aclk                 (clk),
        .s_axi_aresetn              (rst_n),

        /* AXI Write Address Channel Signals */
        .s_axi_awaddr               (s_axi_awaddr),
        .s_axi_awprot               (),
        .s_axi_awvalid              (s_axi_awvalid),
        .s_axi_awready              (s_axi_awready),

        /* AXI Write Data Channel Signals */
        .s_axi_wdata                (s_axi_wdata),
        .s_axi_wstrb                (s_axi_wstrb),
        .s_axi_wvalid               (s_axi_wvalid),
        .s_axi_wready               (s_axi_wready),

        /* AXI Write Response Channel Signals */
        .s_axi_bresp                (s_axi_bresp),
        .s_axi_bvalid               (s_axi_bvalid),
        .s_axi_bready               (s_axi_bready),

        /* AXI Read Address Channel Signals */
        .s_axi_araddr               (s_axi_araddr),
        .s_axi_arprot               (),
        .s_axi_arvalid              (s_axi_arvalid),
        .s_axi_arready              (s_axi_arready),

        /* AXI Read Data Channel Signals */
        .s_axi_rdata                (s_axi_rdata),
        .s_axi_rresp                (s_axi_rresp),
        .s_axi_rvalid               (s_axi_rvalid),
        .s_axi_rready               (s_axi_rready),

        /* SRAM Protocol */
        .i_reset_bar_out              (i_reset_bar),
        .i_clock_out                  (i_clock),
        .i_enable_out                 (i_enable),
        .i_address_out                (i_address),
        .i_read_data_in               (i_read_data),
        .i_write_data_out             (i_write_data),
        .i_read_write_bar_out         (i_read_write_bar),
        .i_read_complete_in           (i_read_complete),
        .i_write_complete_in          (i_write_complete)
    );

    `define SRAM_MEM
//    `define L1_COUNTER
    

    `ifdef SRAM_MEM
        sram_mem    inst0_sram_mem(
            .reset_bar_in           (i_reset_bar),
            .clock_in               (i_clock),
            .enable_in              (i_enable),
            .address_in             (i_address),
            .read_data_out          (i_read_data),
            .write_data_in          (i_write_data),
            .read_write_bar_in      (i_read_write_bar),
            .read_complete_out      (i_read_complete),
            .write_complete_out     (i_write_complete)
        );
    `endif

    `ifdef L1_COUNTER
        /* Instantiate l1_counter module */

        wire                  [7:0] i_led_output;

        l1_counter_regbank inst0_l1_counter(
            .reset_bar_in           (i_reset_bar),
            .clock_in               (i_clock),
            .enable_in              (i_enable),
            .address_in             (i_address),
            .read_data_out          (i_read_data),
            .write_data_in          (i_write_data),
            .read_write_bar_in      (i_read_write_bar),
            .read_complete_out      (i_read_complete),
            .write_complete_out     (i_write_complete),
            .led_output             (i_led_output)
        );

        assign zed_board_led_1 = i_led_output[1];
        assign zed_board_led_2 = i_led_output[2];
        assign zed_board_led_3 = i_led_output[3];
        assign zed_board_led_4 = i_led_output[4];
        assign zed_board_led_5 = i_led_output[5];
        assign zed_board_led_6 = i_led_output[6];
        assign zed_board_led_7 = i_led_output[7];

    `endif    

    always @(posedge clk or negedge rst_n)
    begin
        if(!rst_n)
            up_counter <= {1'b1,26'd0};
        else
            up_counter <= up_counter + 1'b1;
    end
    
    assign zed_board_led_0 = up_counter[26];

endmodule

