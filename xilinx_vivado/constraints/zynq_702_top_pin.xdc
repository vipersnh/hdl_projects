#------------------------------------------------------------------------------
# PIN Constraints
#------------------------------------------------------------------------------

set_property PACKAGE_PIN  P17 [get_ports zed_board_led_0]
set_property PACKAGE_PIN  P18 [get_ports zed_board_led_1]
set_property PACKAGE_PIN  W10 [get_ports zed_board_led_2]
set_property PACKAGE_PIN  V7  [get_ports zed_board_led_3]
set_property PACKAGE_PIN  W5  [get_ports zed_board_led_4]
set_property PACKAGE_PIN  W17 [get_ports zed_board_led_5]
set_property PACKAGE_PIN  D15 [get_ports zed_board_led_6]
set_property PACKAGE_PIN  E15 [get_ports zed_board_led_7]

#------------------------------------------------------------------------------
# IO STANDARD
#------------------------------------------------------------------------------

set_property IOSTANDARD LVCMOS25 [get_ports zed_board_led_0]
set_property IOSTANDARD LVCMOS25 [get_ports zed_board_led_1]
set_property IOSTANDARD LVCMOS25 [get_ports zed_board_led_2]
set_property IOSTANDARD LVCMOS25 [get_ports zed_board_led_3]
set_property IOSTANDARD LVCMOS25 [get_ports zed_board_led_4]
set_property IOSTANDARD LVCMOS25 [get_ports zed_board_led_5]
set_property IOSTANDARD LVCMOS25 [get_ports zed_board_led_6]
set_property IOSTANDARD LVCMOS25 [get_ports zed_board_led_7]




