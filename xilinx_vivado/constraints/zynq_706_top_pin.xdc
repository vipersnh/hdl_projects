#------------------------------------------------------------------------------
# PIN Constraints
#------------------------------------------------------------------------------

set_property PACKAGE_PIN Y21 [get_ports zed_board_led_0]
set_property PACKAGE_PIN  G2 [get_ports zed_board_led_1]
set_property PACKAGE_PIN W21 [get_ports zed_board_led_2]
set_property PACKAGE_PIN A17 [get_ports zed_board_led_3]

#------------------------------------------------------------------------------
# IO STANDARD
#------------------------------------------------------------------------------

set_property IOSTANDARD LVCMOS18 [get_ports zed_board_led_0]
set_property IOSTANDARD LVCMOS18 [get_ports zed_board_led_1]
set_property IOSTANDARD LVCMOS18 [get_ports zed_board_led_2]
set_property IOSTANDARD LVCMOS18 [get_ports zed_board_led_3]

