#------------------------------------------------------------------------------
# PIN Constraints
#------------------------------------------------------------------------------

set_property PACKAGE_PIN T22 [get_ports zed_board_led_0]
set_property PACKAGE_PIN T21 [get_ports zed_board_led_1]
set_property PACKAGE_PIN U22 [get_ports zed_board_led_2]
set_property PACKAGE_PIN U21 [get_ports zed_board_led_3]
set_property PACKAGE_PIN V22 [get_ports zed_board_led_4]
set_property PACKAGE_PIN W22 [get_ports zed_board_led_5]
set_property PACKAGE_PIN U19 [get_ports zed_board_led_6]
set_property PACKAGE_PIN U14 [get_ports zed_board_led_7]

#------------------------------------------------------------------------------
# IO STANDARD
#------------------------------------------------------------------------------

set_property IOSTANDARD LVCMOS25 [get_ports zed_board_led_0]
set_property IOSTANDARD LVCMOS25 [get_ports zed_board_led_1]
set_property IOSTANDARD LVCMOS25 [get_ports zed_board_led_2]
set_property IOSTANDARD LVCMOS25 [get_ports zed_board_led_3]
set_property IOSTANDARD LVCMOS25 [get_ports zed_board_led_4]
set_property IOSTANDARD LVCMOS25 [get_ports zed_board_led_5]
set_property IOSTANDARD LVCMOS25 [get_ports zed_board_led_6]
set_property IOSTANDARD LVCMOS25 [get_ports zed_board_led_7]


