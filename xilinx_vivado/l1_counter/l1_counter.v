module l1_counter (
    reset_bar_in,
    clock_in,
    counter_en,
    counter_mode,
    count_intr_en,
    count_cmp_intr_en,
    counter_clocks_per_count,
    cmp_count_0,
    cmp_count_1,
    counter_max_0,
    counter_max_1,
    count_main_out_0,
    count_main_out_1,
    count_base_out,
    led_0_en,
    led_0_counter_bit,
    led_1_en,
    led_1_counter_bit,
    led_2_en,
    led_2_counter_bit,
    led_3_en,
    led_3_counter_bit,
    led_4_en,
    led_4_counter_bit,
    led_5_en,
    led_5_counter_bit,
    led_6_en,
    led_6_counter_bit,
    led_7_en,
    led_7_counter_bit,
    led_output,
);
    input wire                             reset_bar_in;
    input wire                             clock_in;
    input wire                             counter_en;
    input wire                             counter_mode;
    input wire                             count_intr_en;
    input wire                             count_cmp_intr_en;
    input wire                      [31:0] counter_clocks_per_count;
    input wire                      [31:0] cmp_count_0;
    input wire                      [31:0] cmp_count_1;
    input wire                      [31:0] counter_max_0;
    input wire                      [31:0] counter_max_1;
    output wire                     [31:0] count_main_out_0;
    output wire                     [31:0] count_main_out_1;
    output reg                      [31:0] count_base_out;
    input wire                             led_0_en;
    input wire                      [ 5:0] led_0_counter_bit;
    input wire                             led_1_en;
    input wire                      [ 5:0] led_1_counter_bit;
    input wire                             led_2_en;
    input wire                      [ 5:0] led_2_counter_bit;
    input wire                             led_3_en;
    input wire                      [ 5:0] led_3_counter_bit;
    input wire                             led_4_en;
    input wire                      [ 5:0] led_4_counter_bit;
    input wire                             led_5_en;
    input wire                      [ 5:0] led_5_counter_bit;
    input wire                             led_6_en;
    input wire                      [ 5:0] led_6_counter_bit;
    input wire                             led_7_en;
    input wire                      [ 5:0] led_7_counter_bit;
    output wire                     [ 7:0] led_output;

    reg                             [63:0] count_main_out;

    wire                            [63:0] counter_max_value;

    assign counter_max_value = counter_max_0 | (counter_max_1<<32);

    assign  count_main_out_0 = count_main_out[31:0];
    assign  count_main_out_1 = count_main_out[63:32];

    assign led_output[0] = led_0_en & count_main_out[led_0_counter_bit];
    assign led_output[1] = led_1_en & count_main_out[led_1_counter_bit];
    assign led_output[2] = led_2_en & count_main_out[led_2_counter_bit];
    assign led_output[3] = led_3_en & count_main_out[led_3_counter_bit];
    assign led_output[4] = led_4_en & count_main_out[led_4_counter_bit];
    assign led_output[5] = led_5_en & count_main_out[led_5_counter_bit];
    assign led_output[6] = led_6_en & count_main_out[led_6_counter_bit];
    assign led_output[7] = led_7_en & count_main_out[led_7_counter_bit];

    /* Write user code here */
    always @ (posedge clock_in, negedge reset_bar_in) begin
        if (reset_bar_in==1'b0) begin
            count_main_out <= 64'b0;
        end
        else begin
            if (counter_en==1'b1) begin
                /* Count main counter only after count_base_out expires */
                if (count_base_out==32'b0) begin
                    if (counter_mode==1'b0) begin
                        /* Downcounter mode */
                        if (count_main_out) begin
                            count_main_out <= count_main_out - 1'b1;
                        end else begin
                            if (counter_max_value) begin
                                count_main_out <= counter_max_value;
                            end
                            else begin
                                count_main_out <= count_main_out - 64'b1;
                            end
                        end
                    end else begin
                        /* Upcounter mode */
                        if (counter_max_value) begin
                            if (count_main_out < counter_max_value) begin
                                count_main_out <= count_main_out + 64'b1;
                            end
                            else begin
                                count_main_out <= counter_max_value;
                            end
                        end
                        else begin
                            count_main_out <= count_main_out + 64'b1;
                        end
                    end
                end
            end else begin
                if (counter_max_value) begin
                    count_main_out <= counter_max_value;
                end
            end
        end
    end


    always @ (posedge clock_in, negedge reset_bar_in) begin
        if (reset_bar_in==1'b0) begin
            count_base_out <= 32'b0;
        end
        else begin
            if (counter_en) begin
                if (count_base_out==0) begin
                    if (counter_clocks_per_count != 0) begin
                        count_base_out <= counter_clocks_per_count;
                    end
                    else begin
                        count_base_out <= 32'b1;
                    end
                end
                else begin
                    count_base_out <= count_base_out - 32'b1;
                end
            end
            else begin
                count_base_out <= 32'b0;
            end
        end
    end

endmodule
