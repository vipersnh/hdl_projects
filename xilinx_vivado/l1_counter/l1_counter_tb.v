`include "l1_counter.v"
`include "l1_counter_regbank.v"

module l1_counter_tb;

    wire            reset_bar_wire;
    wire            clock_wire;
    wire            read_write_bar_wire;
    wire     [31:0] address_wire;
    wire     [31:0] read_data_wire;
    wire     [31:0] write_data_wire;
    wire            read_completed_wire;
    wire            write_completed_wire;

    reg             reset_bar;
    reg             clock;
    reg             enable;
    reg             read_write_bar;
    reg      [31:0] address;
    reg      [31:0] write_data;

    /* Make a regular pulsing clock. */
    always #1 clock = !clock;

    assign reset_bar_wire   = reset_bar;
    assign clock_wire       = clock;
    assign write_data_wire  = write_data;
    assign enable_wire  = enable;
    assign read_write_bar_wire = read_write_bar;
    assign address_wire = address;
    
    task reset_sram;
        begin
            #0 reset_bar = 0;
            #1 reset_bar = 1;
        end
    endtask

    task write_memory;
        input [31:0] addr;
        input [31:0] w_data;
        begin
            wait (clock);
            #0 enable = 1;
            #0 read_write_bar = 0;
            #0 address = addr;
            #0 write_data = w_data;

            wait (clock & write_completed_wire);
            #0 enable = 0;
            wait (clock & !write_completed_wire);
        end
    endtask

    task read_memory;
        input [31:0] addr;
        begin
            wait (!clock);
            #0 enable = 1;
            #0 read_write_bar = 1;
            #0 address = addr;

            wait (clock & read_completed_wire);
            #0 enable = 0;
            wait (clock & !read_completed_wire);
        end
    endtask

    initial begin
        $dumpfile("l1_counter_tb.vcd");
        $dumpvars(0, l1_counter_tb);
        # 0  clock = 0;
        # 0  reset_bar = 0;
        # 0  enable = 0;
        # 0  read_write_bar = 0;
        # 0  address = 0;
        # 0  write_data = 0;

        reset_sram();
        write_memory(32'd0, 32'hFFFFFFFF);
        read_memory(32'd0);
        #10 read_memory(32'd24);
        #10 read_memory(32'd24);
        #10 read_memory(32'd24);
        #10 read_memory(32'd24);
        # 100 $finish;
    end
    
    l1_counter_regbank l1_counter_dut(
        .reset_bar_in           (reset_bar_wire),
        .clock_in               (clock_wire),
        .enable_in              (enable_wire),
        .address_in             (address_wire),
        .read_data_out          (read_data_wire),
        .write_data_in          (write_data_wire),
        .read_write_bar_in      (read_write_bar_wire),
        .read_complete_out      (read_completed_wire),
        .write_complete_out     (write_completed_wire)
    );
endmodule     
