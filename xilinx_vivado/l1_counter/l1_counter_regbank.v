module l1_counter_regbank (
    reset_bar_in,
    clock_in,
    enable_in,
    address_in,
    read_data_out,
    write_data_in,
    read_write_bar_in,
    read_complete_out,
    write_complete_out,
    led_output
);
    input wire                             reset_bar_in;
    input wire                             clock_in;
    input wire                             enable_in;
    input wire                       [5:0] address_in;
    output reg                      [31:0] read_data_out;
    input wire                      [31:0] write_data_in;
    input wire                             read_write_bar_in;
    output reg                             read_complete_out;
    output reg                             write_complete_out;

    reg                                    counter_en;
    reg                                    counter_mode;
    reg                                    count_intr_en;
    reg                                    count_cmp_intr_en;
    reg                             [31:0] counter_clocks_per_count;
    reg                             [31:0] cmp_count_0;
    reg                             [31:0] cmp_count_1;
    reg                             [31:0] counter_max_0;
    reg                             [31:0] counter_max_1;
    wire                            [31:0] count_main_out_0;
    wire                            [31:0] count_main_out_1;
    wire                            [31:0] count_base_out;
    reg                                    led_0_en;
    reg                             [ 5:0] led_0_counter_bit;
    reg                                    led_1_en;
    reg                             [ 5:0] led_1_counter_bit;
    reg                                    led_2_en;
    reg                             [ 5:0] led_2_counter_bit;
    reg                                    led_3_en;
    reg                             [ 5:0] led_3_counter_bit;
    reg                                    led_4_en;
    reg                             [ 5:0] led_4_counter_bit;
    reg                                    led_5_en;
    reg                             [ 5:0] led_5_counter_bit;
    reg                                    led_6_en;
    reg                             [ 5:0] led_6_counter_bit;
    reg                                    led_7_en;
    reg                             [ 5:0] led_7_counter_bit;
    output wire                     [ 7:0] led_output;

    l1_counter u0_l1_counter (
        .reset_bar_in (reset_bar_in),
        .clock_in (clock_in),
        .counter_en (counter_en),
        .counter_mode (counter_mode),
        .count_intr_en (count_intr_en),
        .count_cmp_intr_en (count_cmp_intr_en),
        .counter_clocks_per_count (counter_clocks_per_count),
        .cmp_count_0 (cmp_count_0),
        .cmp_count_1 (cmp_count_1),
        .counter_max_0 (counter_max_0),
        .counter_max_1 (counter_max_1),
        .count_main_out_0 (count_main_out_0),
        .count_main_out_1 (count_main_out_1),
        .count_base_out (count_base_out),
        .led_0_en (led_0_en),
        .led_0_counter_bit (led_0_counter_bit),
        .led_1_en (led_1_en),
        .led_1_counter_bit (led_1_counter_bit),
        .led_2_en (led_2_en),
        .led_2_counter_bit (led_2_counter_bit),
        .led_3_en (led_3_en),
        .led_3_counter_bit (led_3_counter_bit),
        .led_4_en (led_4_en),
        .led_4_counter_bit (led_4_counter_bit),
        .led_5_en (led_5_en),
        .led_5_counter_bit (led_5_counter_bit),
        .led_6_en (led_6_en),
        .led_6_counter_bit (led_6_counter_bit),
        .led_7_en (led_7_en),
        .led_7_counter_bit (led_7_counter_bit),
        .led_output (led_output)
    );

    reg  state;
    reg  next_state;
    localparam IDLE              = 1'd0;
    localparam READ_WRITE_MODE   = 1'd1;

    always @ (posedge clock_in or negedge reset_bar_in) begin
        if (reset_bar_in==1'b0) begin
            read_data_out <= 32'd0;
            read_complete_out <= 1'd0;
            write_complete_out <= 1'd0;
            state <= IDLE;
        end else begin
            case (state)
                IDLE: begin
                    if (enable_in) begin
                        state <= READ_WRITE_MODE;
                    end else begin
                        state <= IDLE;
                    end
                end
                READ_WRITE_MODE: begin
                    if (read_write_bar_in) begin
                        read_complete_out <= 1'd1;
                        if (!read_complete_out) begin
                            case (address_in) 
                                6'd0: begin
                                    read_data_out <= (counter_en<<0) | (counter_mode<<1) | (count_intr_en<<16) | (count_cmp_intr_en<<17);
                                end
                                6'd4: begin
                                    read_data_out <= (counter_clocks_per_count<<0);
                                end
                                6'd8: begin
                                    read_data_out <= (cmp_count_0<<0);
                                end
                                6'd12: begin
                                    read_data_out <= (cmp_count_1<<0);
                                end
                                6'd16: begin
                                    read_data_out <= (counter_max_0<<0);
                                end
                                6'd20: begin
                                    read_data_out <= (counter_max_1<<0);
                                end
                                6'd24: begin
                                    read_data_out <= (count_main_out_0<<0);
                                end
                                6'd28: begin
                                    read_data_out <= (count_main_out_1<<0);
                                end
                                6'd32: begin
                                    read_data_out <= (count_base_out<<0);
                                end
                                6'd36: begin
                                    read_data_out <= (led_0_en<<0) | (led_0_counter_bit<<1) | (led_1_en<<8) | (led_1_counter_bit<<9) | (led_2_en<<16) | (led_2_counter_bit<<17) | (led_3_en<<24) | (led_3_counter_bit<<25);
                                end
                                6'd40: begin
                                    read_data_out <= (led_4_en<<0) | (led_4_counter_bit<<1) | (led_5_en<<8) | (led_5_counter_bit<<9) | (led_6_en<<16) | (led_6_counter_bit<<17) | (led_7_en<<24) | (led_7_counter_bit<<25);
                                end
                                6'd44: begin
                                    read_data_out <= (led_output<<0);
                                end
                                default: begin
                                    read_data_out <= 32'd0;
                                end
                            endcase
                        end
                    end else begin
                        write_complete_out <= 1'd1;
                        if (!write_complete_out) begin
                            case (address_in) 
                                6'd0: begin
                                    counter_en <= write_data_in[0];
                                    counter_mode <= write_data_in[1];
                                    count_intr_en <= write_data_in[16];
                                    count_cmp_intr_en <= write_data_in[17];
                                end
                                6'd4: begin
                                    counter_clocks_per_count <= write_data_in[31:0];
                                end
                                6'd8: begin
                                    cmp_count_0 <= write_data_in[31:0];
                                end
                                6'd12: begin
                                    cmp_count_1 <= write_data_in[31:0];
                                end
                                6'd16: begin
                                    counter_max_0 <= write_data_in[31:0];
                                end
                                6'd20: begin
                                    counter_max_1 <= write_data_in[31:0];
                                end
                                6'd24: begin
                                end
                                6'd28: begin
                                end
                                6'd32: begin
                                end
                                6'd36: begin
                                    led_0_en <= write_data_in[0];
                                    led_0_counter_bit <= write_data_in[6:1];
                                    led_1_en <= write_data_in[8];
                                    led_1_counter_bit <= write_data_in[14:9];
                                    led_2_en <= write_data_in[16];
                                    led_2_counter_bit <= write_data_in[22:17];
                                    led_3_en <= write_data_in[24];
                                    led_3_counter_bit <= write_data_in[30:25];
                                end
                                6'd40: begin
                                    led_4_en <= write_data_in[0];
                                    led_4_counter_bit <= write_data_in[6:1];
                                    led_5_en <= write_data_in[8];
                                    led_5_counter_bit <= write_data_in[14:9];
                                    led_6_en <= write_data_in[16];
                                    led_6_counter_bit <= write_data_in[22:17];
                                    led_7_en <= write_data_in[24];
                                    led_7_counter_bit <= write_data_in[30:25];
                                end
                                6'd44: begin
                                end
                                default: begin
                                end
                            endcase
                        end
                    end

                    if (enable_in) begin
                        state <= READ_WRITE_MODE;
                    end else begin
                        read_complete_out <= 1'd0;
                        write_complete_out <= 1'd0;
                        state <= IDLE;
                    end
                end
            endcase
        end
    end
endmodule
