
Purpose: Hardware projects to demo MMIO using Xilinx Zynq platform

Requirements:
    - Xilinx Zynq boards (Any of ZC702, ZC706 or Zedboard)
    - Xilinx Vivado 2014.3
    - Xilinx SDK 2014.3
    - Linux machine preferrably running Ubuntu 16.10

Directories: 
    - ./xilinx_vivado/axi2sram
        - Contains axi2sram module and axi driver for test bench
    - ./xilinx_vivado/constraints
        - Contains pin and timing constraints for ZC702, ZC706 and Zedboard
    - ./xilinx_vivado/l1_counter
        - Contains l1_counter module excel sheet and module RTL code
    - ./xilinx_vivado/sram
        - Contains SRAM module and the corresponding test bench
    - ./xilinx_vivado/vivado_screenshot.png
        - Screenshot of the Block Diagram design
    - ./xilinx_vivado/zynq_fpga_top.v
        - Top level module for the project
    - ./zc702
        - Contains Linux related files for ZC702 board
    - ./zc706
        - Contains Linux related files for ZC706 board
    - ./zedboard
        - Contains Linux related files for Zedboard board
    - ./zynq_common
        - Contains commands and linux images which work across boards

SRAM Module :
    - This module was designed for testing purpose, it emulates a simple MMIO based SRAM in FPGA

L1_COUNTER Module :
    - This module was designed for testing purpose, it emulates a simple MMIO based counter in FPGA
